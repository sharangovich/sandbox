require 'minitest/autorun'

require './application'

class MarsRoverRacingTest < MiniTest::Unit::TestCase

  def test_case
    input = <<-EOS
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM
EOS

    output = <<-STRING
Welcome to the Mars Rover Race
Please set plateau weight and height:
Enter rover initial position:
Enter rover command line:
Enter rover initial position:
Enter rover command line:
1 3 N
5 1 E
STRING

    test_output = emulate_console(input) { MarsRoverRacing.new(2).call }
    assert_equal(test_output.string, output)
  end

  def emulate_console(console_input)
    $stdin = StringIO.new(console_input)
    out = StringIO.new
    $stdout = out
    yield
    return out
  ensure
    $stdout = STDOUT
    $stdin = STDIN
  end
end
