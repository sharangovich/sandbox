require 'minitest/autorun'

require './services/compass'
require './services/compass/rotator'

class CompassRotatorTest < MiniTest::Unit::TestCase
  def test_rotate_from_n_to_right
    new_direction = Compass::Rotator.new_direction('N', 'R')
    assert_equal(new_direction, 'E')
  end

  def test_rotate_from_e_to_right
    new_direction = Compass::Rotator.new_direction('E', 'R')
    assert_equal(new_direction, 'S')
  end

  def test_rotate_from_s_to_right
    new_direction = Compass::Rotator.new_direction('S', 'R')
    assert_equal(new_direction, 'W')
  end

  def test_rotate_from_w_to_right
    new_direction = Compass::Rotator.new_direction('W', 'R')
    assert_equal(new_direction, 'N')
  end

  def test_rotate_from_n_to_left
    new_direction = Compass::Rotator.new_direction('N', 'L')
    assert_equal(new_direction, 'W')
  end

  def test_rotate_from_e_to_left
    new_direction = Compass::Rotator.new_direction('E', 'L')
    assert_equal(new_direction, 'N')
  end

  def test_rotate_from_s_to_left
    new_direction = Compass::Rotator.new_direction('S', 'L')
    assert_equal(new_direction, 'E')
  end

  def test_rotate_from_w_to_left
    new_direction = Compass::Rotator.new_direction('W', 'L')
    assert_equal(new_direction, 'S')
  end
end
