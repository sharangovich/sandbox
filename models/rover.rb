class Rover
  attr_accessor :x_coord
  attr_accessor :y_coord
  attr_accessor :direction

  def initialize(x_coord, y_coord, direction)
    @x_coord = x_coord
    @y_coord = y_coord
    @direction = direction
  end

  def to_s
    "#{x_coord} #{y_coord} #{direction}"
  end
end
