class Plateau
  attr_accessor :size_x
  attr_accessor :size_y

  def initialize(size_x, size_y)
    @size_x = size_x
    @size_y = size_y
  end
end
