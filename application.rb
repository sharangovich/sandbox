require './models/plateau'
require './models/rover'
require './services/compass'
require './services/compass/rotator'
require './services/rover_control/mover'

class MarsRoverRacing
  attr_accessor :rovers_count
  attr_accessor :rovers_with_command
  attr_accessor :plateau

  def initialize(rovers_count = 2)
    @rovers_count = rovers_count
    @rovers_with_command = []
  end

  def call
    print_welcome_message
    get_plato_size
    rovers_count.times { collect_rover_data }
    move_rovers_and_print_results
  end

  private

  def print_welcome_message
    puts 'Welcome to the Mars Rover Race'
  end

  def get_plato_size
    puts 'Please set plateau weight and height:'
    plateau_size = gets.chomp.split(' ').map(&:to_i)
    @plateau = Plateau.new(plateau_size[0], plateau_size[1])
  end

  def collect_rover_data
    puts 'Enter rover initial position:'
    rover_position_data = gets.chomp.split(' ')
    puts 'Enter rover command line:'
    rover_command = gets.chomp
    rover = Rover.new(
      rover_position_data[0].to_i,
      rover_position_data[1].to_i,
      rover_position_data[2].upcase
    )
    rovers_with_command << [rover, rover_command]
  end

  def move_rovers_and_print_results
    rovers_with_command.each_with_index do |rover_with_command, index|
      rover = rover_with_command.first
      rover_mover = RoverControl::Mover.new(rover, plateau)
      rover_mover.perform_command(rover_with_command.last)
      puts rover.to_s
    end
  end
end
