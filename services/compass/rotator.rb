module Compass
  class Rotator
    ALLOWED_ROTATE = [
      ROTATE_RIGHT = 'R',
      ROTATE_LEFT = 'L'
    ]

    def self.new_direction(old_direction, rotate)
      current_index = COMPASS_DIRECTIONS.index(old_direction)

      case rotate
      when ROTATE_RIGHT
        if current_index == COMPASS_DIRECTIONS.size - 1
          COMPASS_DIRECTIONS[0]
        else
          COMPASS_DIRECTIONS[current_index + 1]
        end
      when ROTATE_LEFT
        COMPASS_DIRECTIONS[current_index - 1]
      end
    end
  end
end
