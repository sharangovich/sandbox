module RoverControl
  class Mover
    MOVE_COMMAND = 'M'
    VALID_COMMANDS = Compass::Rotator::ALLOWED_ROTATE + [MOVE_COMMAND]
    INVALID_COMMAND_MESSAGE = 'Invalid command input, use M, R or L letters!'

    attr_accessor :rover
    attr_accessor :plateau

    def initialize(rover, plateau)
      @rover = rover
      @plateau = plateau
    end

    def perform_command(command_string)
      test_regex = Regexp.new("^(#{ VALID_COMMANDS.join('|') })*$")
      command_string.upcase!
      raise(ArgumentError, INVALID_COMMAND_MESSAGE) unless command_string =~ test_regex

      command_string.each_char do |command|
        case
        when command == MOVE_COMMAND
          move!
        when Compass::Rotator::ALLOWED_ROTATE.include?(command)
          rotate!(command)
        end
      end
    end

    private

    def move!
      new_x = rover.x_coord
      new_y = rover.y_coord

      case rover.direction
      when Compass::DIRECTION_N
        new_y += 1
        validate_move(new_y)
      when Compass::DIRECTION_E
        new_x += 1
        validate_move(new_x,  plateau.size_x)
      when Compass::DIRECTION_S
        new_y -= 1
        validate_move(new_y,  plateau.size_y)
      when Compass::DIRECTION_W
        new_x -= 1
        validate_move(new_x)
      end

      rover.x_coord = new_x
      rover.y_coord = new_y
    end

    def rotate!(rotate)
      rover.direction = Compass::Rotator.new_direction(rover.direction, rotate)
    end

    def validate_move(coord, max_value = nil)
      return true if  coord >= 0 || (max_value && coord <= max_value)

      raise StandardError, 'Going outside plateau!'
    end
  end
end
