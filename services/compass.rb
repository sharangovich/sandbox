module Compass
  COMPASS_DIRECTIONS = [
    DIRECTION_N = 'N',
    DIRECTION_E = 'E',
    DIRECTION_S = 'S',
    DIRECTION_W = 'W'
  ]
end
